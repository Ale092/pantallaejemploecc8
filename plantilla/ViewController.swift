//
//  ViewController.swift
//  plantilla
//
//  Created by alejandro on 17/03/23.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var contenedorPrincipal: UIView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        /// Se le asigna lo bordes superiores redondos y la sombra al contenedor principal
        self.contenedorPrincipal.roundCorners(corners: [.layerMaxXMinYCorner, .layerMinXMinYCorner], radius: 35.0)
        
        self.contenedorPrincipal.sombrasContenedorPrincipal()
        
        colorBarraStatus()
    }
    
}








extension UIView {
    /**
     Función que permite redondear algun borde en especifico de una vista
     */
    func roundCorners(corners:CACornerMask, radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = corners
        
    }
    
    func sombrasContenedorPrincipal(){
        layer.shadowColor = UIColor.black.cgColor
        layer.masksToBounds = false
        layer.shadowOpacity = 0.8
        layer.shadowOffset = .zero
        layer.shadowRadius = 15
    }
}


extension UIViewController{
    /**
     Método para cambiar el color de la barra de estado de la app
     */
    func colorBarraStatus(colorBarraStatus ColorBarra : UIColor =  UIColor(red: 48/255, green: 52/255, blue: 54/255, alpha: 1)){
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            print("statusBarHeight \(statusBarHeight)")
           
            //let statusBarHeight2 = view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 20
            
            let statusbarView = UIView()
            
            statusbarView.backgroundColor = ColorBarra
            view.addSubview(statusbarView)
          
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
          
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor.black
        }
    }
}
